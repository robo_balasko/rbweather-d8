<?php namespace Drupal\rb_weather\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rb_weather\Services\WeatherInterface;
use Drupal\rb_weather\Storage\CustomStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WeatherBLock
 *
 * @Block (
 *     id = "rb_weather_block",
 *     admin_label = @Translation("RBWeather block"),
 * )
 *
 * @package Drupal\rb_weather\Plugin\Block
 */
class WeatherBlock extends BlockBase implements ContainerFactoryPluginInterface
{
    private $wunderground;

    private $locationsStorage;

    public function __construct(array $configuration,
                                $plugin_id,
                                $plugin_definition,
                                WeatherInterface $wunderground,
                                CustomStorageInterface $locationsStorage)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);

        $this->wunderground = $wunderground;
        $this->locationsStorage = $locationsStorage;
    }

    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('rb_weather.wunderground'),
            $container->get('rb_weather.locations_storage')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $config = $this->getConfiguration();

        $locations = $this->locationsStorage->getAll('id', ['id', 'name', 'link']);

        $selected_locations = isset($config['locations'])
            ? $config['locations']
            : [];

        $weather_reports = array();

        foreach ($selected_locations as $selected_location) {
            if (in_array($selected_location, array_keys($locations))) {
                $weather_reports[] = $this->wunderground->getCurrentWeather($locations[$selected_location]['link']);
            }
        }

        return [
            '#theme' => 'weather_block',
            '#locations' => $weather_reports,
            '#cache' => ['max-age' => 0],
            '#attached' => [
                'library' => [
                    'rb_weather/weather-block-css',
                ]
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state)
    {
        parent::blockForm($form, $form_state);

        $configuration = $this->getConfiguration();
        $configured_locations = $this->getSelectedLocations($configuration['locations']);
        $locations = $this->locationsStorage->getAll('id', ['id', 'name', 'link']);

        if (empty($locations)) {
            drupal_set_message($this->t('Click <a href="@link">here</a> to add some locations first.', [
                '@link' => '/admin/config/services/rb_weather',
            ]), 'warning');
        }

        $select = [];

        if (!empty($locations)) {
            foreach ($locations as $key => $config_item) {
                $select[$key] = $config_item['name'];
            }
        }

        $form['locations'] = array(
            '#title' => $this->t('Locations'),
            '#type' => 'checkboxes',
            '#options' => $select,
            '#default_value' => $configured_locations,
            '#required' => TRUE,
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();

        $this->setConfigurationValue('locations', $this->getSelectedLocations($values['locations']));
    }

    private function getSelectedLocations($locations)
    {
        $selected = [];

        foreach ($locations as $location) {
            if ($location !== 0) {
                $selected[] = $location;
            }
        }

        return $selected;
    }

}
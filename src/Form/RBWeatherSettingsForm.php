<?php namespace Drupal\rb_weather\Form;


use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\rb_weather\Storage\CustomStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RBWeatherSettingsForm extends ConfigFormBase implements ContainerInjectionInterface
{
    private $locationsStorage;

    /**
     * RBWeatherSettingsForm constructor.
     *
     * @param ConfigFactoryInterface $config
     * @param CustomStorageInterface $locationsStorage
     */
    public function __construct(ConfigFactoryInterface $config, CustomStorageInterface $locationsStorage)
    {
        parent::__construct($config);

        $this->locationsStorage = $locationsStorage;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('config.factory'),
            $container->get('rb_weather.locations_storage')
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
            'rb_weather.settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'rb_weather_admin_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['locations'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Locations'),
            '#collapsible' => FALSE,
            '#collapsed' => FALSE,
            '#tree' => TRUE,
            '#prefix' => '<div id="locations">',
            '#suffix' => '</div>',
        ];


        $locations = $this->locationsStorage->getAll('id', ['id', 'name', 'link']);
        $locations = array_values($locations);

        $citiesCount = count($locations);
        $citiesCount++;

        for ($i = 0; $i < $citiesCount; $i++) {
            $form['locations'][$i]['location'] = [
                '#type' => 'textfield',
                '#title' => $this->t('Location #@nr', ['@nr' => $i + 1]),
                '#default_value' => isset($locations[$i])
                    ? $locations[$i]['name'] . ' - ' . $locations[$i]['link']
                    : '',
                '#autocomplete_route_name' => 'rb_weather.locations.autocomplete',
            ];
        }

        return parent::buildForm($form, $form_state);
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);

        $locations = $form_state->getValue('locations');

        foreach ($locations as $key => $location) {
            if (strpos($location['location'], '/q/zmw/') == -1) {
                $form_state->setError($form['locations'], $this->t('Some of your locations are missing the ZMW number.'));
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $locations = $form_state->getValue('locations');

        $this->locationsStorage->deleteAll();

        foreach ($locations as $key => $location) {
            if (empty($location['location'])) {
                unset($locations[$key]);
            } else {
                $locations[$key] = str_replace('"', '', $location);
                $locations[$key] = explode(' - ', $locations[$key]['location']);

                $this->locationsStorage->insert([
                    'name' => $locations[$key][0],
                    'link' => $locations[$key][1],
                ]);
            }
        }
    }

}
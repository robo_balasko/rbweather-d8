<?php namespace Drupal\rb_weather\Storage;


use Drupal\Core\Database\Database;

class BaseCustomStorage implements CustomStorageInterface
{
    protected $baseTable;

    protected $database;

    public function __construct($baseTable)
    {
        $this->baseTable = $baseTable;
        $this->database = Database::getConnection();
    }

    public function getById($id, array $columns = [])
    {
        return $this->database->select($this->baseTable, 't')
            ->fields('t', $columns)
            ->execute()
            ->fetchAssoc();
    }

    public function getAll($assoc_key, array $columns)
    {
        if (!in_array($assoc_key, $columns)) {
            throw new InvalidAssociationKeyException();
        }

        return $this->database->select($this->baseTable, 't')
            ->fields('t', $columns)
            ->execute()
            ->fetchAllAssoc($assoc_key, \PDO::FETCH_ASSOC);
    }

    public function insert(array $data)
    {
        return $this->database->insert($this->baseTable)
            ->fields($data)
            ->execute();
    }

    public function update($id, array $data)
    {
        return $this->database->update($this->baseTable)
            ->fields($data)
            ->condition('id', $id)
            ->execute();
    }

    public function delete($id)
    {
        return $this->database->delete($this->baseTable)
            ->condition('id', $id)
            ->execute();
    }

    public function deleteAll()
    {
        return $this->database->truncate($this->baseTable)->execute();
    }
}
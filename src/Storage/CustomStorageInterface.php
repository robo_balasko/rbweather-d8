<?php namespace Drupal\rb_weather\Storage;


interface CustomStorageInterface
{
    public function getById($id, array $columns = []);

    public function getAll($assoc_key, array $columns);

    public function insert(array $data);

    public function update($id, array $data);

    public function delete($id);

    public function deleteAll();
}
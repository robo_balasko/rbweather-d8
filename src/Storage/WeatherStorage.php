<?php namespace Drupal\rb_weather\Storage;


class WeatherStorage extends BaseCustomStorage
{
    public function __construct()
    {
        parent::__construct('rb_weather_locations');
    }
}
<?php namespace Drupal\rb_weather\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\rb_weather\Services\WeatherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class LocationsController extends ControllerBase
{
    /**
     * @var WeatherInterface
     */
    private $wunderground;

    /**
     * LocationsController constructor.
     *
     * @param WeatherInterface $wunderground
     */
    public function __construct(WeatherInterface $wunderground)
    {
        $this->wunderground = $wunderground;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('rb_weather.wunderground')
        );
    }


    public function locationsAutocomplete()
    {
        $search = \Drupal::request()->get('q');

        $locations = $this->wunderground->getLocationHints($search);

        return new JsonResponse($locations);
    }
}
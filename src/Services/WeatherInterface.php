<?php namespace Drupal\rb_weather\Services;


interface WeatherInterface
{
    /**
     * Provides a list of locations that
     * match the search criteria when searching for a location
     * to get weather forecast for.
     *
     * @param $search
     *
     * @return mixed
     */
    public function getLocationHints($search);

    /**
     * Fetches the current weather conditions at a location.
     *
     * @param $location
     *
     * @return mixed
     */
    public function getCurrentWeather($location);

    /**
     * Fetches the weather forecast for a location.
     *
     * @param $location
     *
     * @return mixed
     */
    public function getWeatherForecast($location);
}
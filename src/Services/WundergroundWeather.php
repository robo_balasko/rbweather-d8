<?php namespace Drupal\rb_weather\Services;


class WundergroundWeather implements WeatherInterface
{
    const FORMAT = '.json';

    const API_KEY = '17e8427c6b3816cd';

    const AUTOCOMPELTE_URI = 'http://autocomplete.wunderground.com/aq?query=';

    const CURRENT_WEATHER_URI = 'http://api.wunderground.com/api/';

    /**
     * {@inheritdoc}
     */
    public function getLocationHints($search)
    {
        $response = file_get_contents(self::AUTOCOMPELTE_URI . urlencode($search));
        $response = json_decode($response);

        $locations = array();

        if (isset($response->RESULTS)) {
            foreach ($response->RESULTS as $location) {
                $locations[] = [
                    'label' => $location->name,
                    'value' => $location->name . ' - ' . $location->l,
                ];
            }
        }

        return $locations;
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrentWeather($location)
    {
        $response = file_get_contents(self::CURRENT_WEATHER_URI . self::API_KEY . '/conditions/' . $location . self::FORMAT);
        $response = json_decode($response);

        $current = $response->current_observation;

        return [
            'name' => $current->display_location->full,
            'date' => date('d.m.Y H:i', $current->observation_epoch),
            'temp' => $current->temp_c,
            'icon' => $current->icon_url,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getWeatherForecast($location)
    {
        // TODO: Implement getWeatherForecast() method.
    }
}